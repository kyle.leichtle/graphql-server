//import Express from 'express';
//import GraphHTTP from 'express-graphql';
//import Schema from './schema';

const Express = require('express');
const GraphHTTP = require('express-graphql');
const Schema = require('./schema');


// const webpackConfig = require("../webpack.config");
// const webpack = require("webpack");
// const webpackMiddleware = require("webpack-dev-middleware");


const PORT = 4000;



const app = Express();



// app.use(webpackMiddleware(webpack(webpackConfig)));

app.use('/graphql',
    GraphHTTP({
        schema: Schema,
        pretty: true,
        graphiql: true
    })
);

app.listen(PORT,  function() {
    console.log('listening on port', PORT);
});

///////////////////////////////////////////


