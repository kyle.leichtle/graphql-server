//import {
//    GraphQLObjectType,
//    GraphQLString,
//    GraphQLInt,
//    GraphQLSchema,
//    GraphQLList,
//    GraphQLBoolean,
//    GraphQLNonNull
//    } from 'graphql';
//
//import Db from './db';

const graphql = require('graphql');
const GraphQLObjectType = graphql.GraphQLObjectType;
const GraphQLString = graphql.GraphQLString;
const GraphQLInt = graphql.GraphQLInt;
const GraphQLSchema = graphql.GraphQLSchema;
const GraphQLList = graphql.GraphQLList;
const GraphQLBoolean = graphql.GraphQLBoolean;
const GraphQLNonNull = graphql.GraphQLNonNull;

const Db = require('./db');

const User = new GraphQLObjectType({
    name: 'User',
    description: 'This represents a Person',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve (user) {
                    return user.id;
                }
            },
            regNum: {
                type: GraphQLInt,
                resolve (user) {
                    return user.regNum;
                }
            },
            email: {
                type: GraphQLString,
                resolve (user) {
                    return user.email;
                }
            },
            firstName: {
                type: GraphQLString,
                resolve (user) {
                    return user.firstName;
                }
            },
            lastName: {
                type: GraphQLString,
                resolve (user) {
                    return user.lastName;
                }
            },
            charClass: {
                type: GraphQLString,
                resolve (user) {
                    return user.charClass;
                }
            },
            xp: {
                type: GraphQLInt,
                resolve (user) {
                    return user.xp;
                }
            },
            feets: {
                type: GraphQLInt,
                resolve (user) {
                    return user.feets;
                }
            },

            posts: {
                type: new GraphQLList(Post),
                resolve (user) {
                    return user.getPosts();
                }
            },
            perks: {
                type: new GraphQLList(Perk),
                resolve (user) {
                    return user.getPerks();
                }
            },
            customizations: {
                type: new GraphQLList(Customization),
                resolve (user) {
                    return user.getCustomizations();
                }
            },
            attendingNextYear: {
                type: GraphQLBoolean,
                resolve (user) {
                    return user.attendingNextYear;
                }
            },
            password: {
                type: GraphQLString,
                resolve (user) {
                    return user.password;
                }
            },
        }
    }
})
;

const Post = new GraphQLObjectType({
    name: 'Post',
    description: 'Blog post',
    fields: () => {
        return {
            title: {
                type: GraphQLString,
                resolve (post) {
                    return post.title;
                }
            },
            content: {
                type: GraphQLString,
                resolve (post) {
                    return post.content;
                }
            },
            user: {
                type: User,
                resolve (post) {
                    return post.getPUser();
                }
            }
        };
    }
});

const Perk = new GraphQLObjectType({
    name: 'Perk',
    description: 'Perk test',
    fields: () => {
        return {
            title: {
                type: GraphQLString,
                resolve (perk) {
                    return perk.title;
                }
            },
            description: {
                type: GraphQLString,
                resolve (perk) {
                    return perk.description;
                }
            },
            person: {
                type: User,
                resolve (perk) {
                    return perk.getUser();
                }
            }
        };
    }
});

const Customization = new GraphQLObjectType({
    name: 'Customization',
    description: 'Perk test',
    fields: () => {
        return {
            title: {
                type: GraphQLString,
                resolve (customization) {
                    return customization.title;
                }
            },
            description: {
                type: GraphQLString,
                resolve (customization) {
                    return customization.description;
                }
            },
            user: {
                type: User,
                resolve (customization) {
                    return customization.getUser();
                }
            }
        };
    }
});

const Query = new GraphQLObjectType({
    name: 'Query',
    description: 'Root query object',
    fields: () => {
        return {
            users: {
                type: new GraphQLList(User),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    attendingNextYear: {
                        type: GraphQLBoolean
                    },
                    firstName: {
                        type: GraphQLString
                    },
                    email: {
                        type: GraphQLString
                    }
                },
                resolve (root, args) {
                    return Db.models.user.findAll({
                        where: args,
                        order: [
                            ['id', 'DESC'],
                        ]
                    });
                }
            },
            posts: {
                type: new GraphQLList(Post),
                resolve (root, args) {
                    return Db.models.post.findAll({where: args});
                }
            },
            perks: {
                type: new GraphQLList(Perk),
                resolve (root, args) {
                    return Db.models.perk.findAll({where: args});
                }
            },
            customizations: {
                type: new GraphQLList(Perk),
                resolve (root, args) {
                    return Db.models.customization.findAll({where: args});
                }
            }
        };
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutations',
    description: 'Functions to set stuff',
    fields: () => {
        return {
            addUser: {
                type: User,
                args: {
                    firstName: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    lastName: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    email: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    charClass: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    regNum: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    xp: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    feets: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    attendingNextYear: {
                        type: new GraphQLNonNull(GraphQLBoolean)
                    }

                },
                resolve (source, args) {
                    return Db.models.user.create({
                        firstName: args.firstName,
                        lastName: args.lastName,
                        email: args.email.toLowerCase(),
                        charClass: args.charClass,
                        regNum: args.regNum,
                        xp: args.xp,
                        feets: args.feets,
                        attendingNextYear: args.attendingNextYear
                    });
                }
            },
            toggleUserAttendance: {
                type: User,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    attendingNextYear: {
                        type: new GraphQLNonNull(GraphQLBoolean)
                    }
                },
                resolve (source, args) {
                    return Db.models.user.update({
                        attendingNextYear: args.attendingNextYear,
                    },
                    {
                        where: {
                            id: args.id
                        }
                    });
                }
            },
            removeUser: {
                type: User,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve (source, args) {
                    return Db.models.user.destroy({
                        where: {
                            id: args.id
                        }
                    })
                }
            }
        };
    }
});

const Schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});

module.exports = Schema;
//export default Schema;